# MifosX Community App 

This is the default web application built on top of the MifosX platform for the mifos user community. It is a Single-Page App (SPA) written in web standard technologies like JavaScript, CSS and HTML5. It leverages common popular frameworks/libraries such as AngularJS, Bootstrap and Font Awesome.


### Docker

This project publishes a Docker image (since #[3112](https://github.com/openMF/community-app/issues/3112)) available on https://hub.docker.com/r/openmf/community-app/.  Our [Dockerfile](Dockerfile) uses a Ruby and Node.JS base image to build the current repo and deploy the app on Nginx, which is exposed on port 80 within the container.  It can be used like this to access the webapp on http://localhost:9090 in your browser:

    docker run --name community-app -it -p 9090:80 mifos-maxfin-community-app


To locally build this Docker image from source (after `git clone` this repo), run:
```
docker build -t mifos-maxfin-community-app .
```
You can then run a Docker Container from the image above like this:
```
docker run --name mifos-maxfin-ui -it -d -p 80:80 mifos-maxfin-community-app
```

Access the webapp on http://localhost in your browser.


### Compile sass to css

```
grunt compass:dev
```
## Running the tests

Just open test/SpecRunner.html in the browser.

## Getting Started doc

https://docs.google.com/document/d/1oXQ2mNojyDFkY_x4RBRPaqS-xhpnDE9coQnbmI3Pobw/edit#heading=h.vhgp8hu9moqn

## Contribution guidelines

Please read the <a href="https://github.com/openMF/community-app/blob/develop/Contributing.md" >contribution guidelines</a>

Note: This application will hit the demo server by default.
